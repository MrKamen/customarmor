package me.mrkamen.customarmor;

import de.tr7zw.nbtapi.NBTItem;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import javax.annotation.Nullable;
import java.util.*;

public class ParserConfigUtils {

    private FileConfiguration config;

    public ParserConfigUtils(FileConfiguration config) {
        this.config = config;
    }

    @Nullable
    public String getName(String itemName) {
        return config.getString("items." + itemName + ".displayname").replaceAll("&", "§");
    }


    public List<String> getLore(String itemType) {
        List<String> lore;
        lore = config.getStringList("items." + itemType + ".lore");

        List<String> newLore = new ArrayList<>();

        lore.forEach(str -> newLore.add(str.replaceAll("&", "§")));

        return newLore;
    }


    public Map<Enchantment, Integer> getEnchantments(String itemName) {
        Map<Enchantment, Integer> enchantments = new HashMap<>();

        for (String enchantment : config.getStringList("items." + itemName + ".enchantments")) {

            String[] args = enchantment.split(":");


            enchantments.put(Enchantment.getByName(args[0].toUpperCase()), Integer.parseInt(args[1]));
        }

        return enchantments;
    }


    public Collection<PotionEffect> getPotions(ItemStack item) {
        Collection<PotionEffect> effects = new ArrayList<>();

        NBTItem nbtItem = new NBTItem(item);

        String potionList = nbtItem.getString("CustomArmor");

        if (potionList.length() != 0) {
            for (String potion : potionList.split(";")) {
                String[] args = potion.split(":");

                effects.add(new PotionEffect(PotionEffectType.getByName(args[0]),
                        Integer.MAX_VALUE,
                        Integer.parseInt(args[1]) - 1
                ));

            }
        }

        return effects;
    }


    public ItemStack addPotionToItem(ItemStack itemStack, String itemName) {
        NBTItem nbtItem = new NBTItem(itemStack);

        StringBuilder potionsList = new StringBuilder();

        for (String potion : config.getStringList("items." + itemName + ".addpotions")) {
            potionsList.append(potion).append(";");
        }

        nbtItem.setString("CustomArmor", potionsList.toString());

        return nbtItem.getItem();
    }

}
