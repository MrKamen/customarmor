package me.mrkamen.customarmor;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.*;

import static org.bukkit.potion.PotionEffectType.*;

public class CustomArmorCommands implements CommandExecutor {

    private CustomArmor Main;

    private FileConfiguration config;
    private ParserConfigUtils parserConfigUtils;
    private Map<String, ItemStack> items = new HashMap<>();

    public CustomArmorCommands(FileConfiguration config, CustomArmor customArmor) {
        this.Main = customArmor;
        this.config = config;
        parserConfigUtils = new ParserConfigUtils(config);
        startSheduler();

        for (String item : config.getConfigurationSection("items").getKeys(false)) {
            items.put(item, createCustomItem(item));
        }
    }


    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {

        if (commandSender instanceof Player) {
            if (!commandSender.hasPermission("customarmor.give")) {
                commandSender.sendMessage(config.getString("noPermission").replaceAll("&", "§"));
                return true;
            }
        }

        if (args.length != 2) {
            commandSender.sendMessage("&fИспользуйте: &a/customarmor player item".replaceAll("&", "§"));
            return true;
        }

        if (!items.containsKey(args[1])) {
            commandSender.sendMessage(config.getString("notFoundItem").replaceAll("&", "§"));
            return true;
        }

        Player player = Bukkit.getPlayerExact(args[0]);


        if (player == null) {
            commandSender.sendMessage(config.getString("offlinePlayer").replaceAll("&", "§"));
            return true;
        }

        player.getInventory().addItem(items.get(args[1]));
        commandSender.sendMessage(config.getString("giveItemPlayer").replaceAll("&", "§"));

        return true;
    }


    private ItemStack createCustomItem(String itemName) {
        ItemStack stack = new ItemStack(Material.getMaterial
                (config.getString("items." + itemName + ".type")),
                1
        );

        ItemMeta itemMeta = stack.getItemMeta();
        String name = parserConfigUtils.getName(itemName);

        if (name != null) {
            itemMeta.setDisplayName(parserConfigUtils.getName(itemName));
        }

        parserConfigUtils.getEnchantments(itemName)
                .forEach((enchantName, level) -> itemMeta.addEnchant(enchantName, level, true));

        itemMeta.setLore(parserConfigUtils.getLore(itemName));
        stack.setItemMeta(itemMeta);

        stack = parserConfigUtils.addPotionToItem(stack, itemName);

        return stack;
    }


    public void appendEffects(Player player) {
        PlayerInventory inventory = player.getInventory();

        List<ItemStack> list = new ArrayList<>();

        String mainHand = inventory.getItemInMainHand().getType().toString();
        String offHand = inventory.getItemInOffHand().getType().toString();

        list.add(inventory.getHelmet());
        list.add(inventory.getChestplate());
        list.add(inventory.getLeggings());
        list.add(inventory.getBoots());

        if (!(mainHand.contains("HELMET")
                || mainHand.contains("CHESTPLATE")
                || mainHand.contains("LEGGINGS")
                || mainHand.contains("BOOTS"))) {
            list.add(inventory.getItemInMainHand());
        }

        if (!(offHand.contains("HELMET")
                || offHand.contains("CHESTPLATE")
                || offHand.contains("LEGGINGS")
                || offHand.contains("BOOTS"))) {
            list.add(inventory.getItemInOffHand());
        }

        Collection<PotionEffect> effects = null;
        Collection<PotionEffect> playerActiveEffects = player.getActivePotionEffects();

        for (ItemStack itemStack : list) {
            Collection<PotionEffect> item = getItemEffects(itemStack);
            if (item != null) {
                if (effects == null) {
                    effects = item;
                } else {
                    effects.addAll(item);
                }
            }
        }

        Set<String> itemEffectsName = new HashSet<>();
        if (effects != null) {
            for (PotionEffect effect : effects) {
                itemEffectsName.add(effect.getType().getName());
            }
        }

        for (PotionEffect effect : playerActiveEffects) {
            if (effect.getDuration() > 9990 * 20) {
                if ((effects == null || !itemEffectsName.contains(effect.getType().getName()))
                        && !effect.getType().getName().equals("INVISIBILITY")) {
//                    System.out.println("DEBUG#1:" + effect.getType().getName());
//                    System.out.println("DEBUG#2: " + (effect.getType() != INVISIBILITY));
                    player.removePotionEffect(effect.getType());
                }
            }
        }

        playerActiveEffects = player.getActivePotionEffects();
        Set<String> playerEffectsAfterRemove = new HashSet<>();
        if (playerActiveEffects != null) {
            for (PotionEffect playerActiveEffect : playerActiveEffects) {
                playerEffectsAfterRemove.add(playerActiveEffect.getType().getName());
            }
        }

        if (effects != null)
            for (PotionEffect effect : effects) {
                if (!(config.getStringList("block-remove-effects").contains(effect.getType().getName())
                        && playerEffectsAfterRemove.contains(effect.getType().getName()))) {
                    if (player.getGameMode() != GameMode.CREATIVE)
                    player.addPotionEffect(effect, true);
                }
            }
    }

    public Collection<PotionEffect> getItemEffects(ItemStack item) {
        Collection<PotionEffect> potions = null;

        if (item != null && item.getType() != Material.AIR) {
            potions = parserConfigUtils.getPotions(item);
        }
        return potions;
    }

    public void startSheduler() {
        Bukkit.getScheduler().runTaskTimer(Main, () -> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                appendEffects(player);
            }
        }, 5, 5);
    }

}
