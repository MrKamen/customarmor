package me.mrkamen.customarmor;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerGameModeChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class CustomArmor extends JavaPlugin {

    @Override
    public void onEnable() {
        saveDefaultConfig();

        System.out.println("CustomArmor enable");

        FileConfiguration config = getConfig();

        this.getCommand("customarmor").setExecutor(new CustomArmorCommands(config, this));
    }

    @Override
    public void onDisable() {
        System.out.println("CustomArmor disable");
    }
}
